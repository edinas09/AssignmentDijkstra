import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class TestDijkstraAlgorithm {

     List<Nodes> nodes;
     List<Edge> edges;
     
   // TESTANDO ...
    public  void testeExcutar(Nodes source ) { //melhorar e colocar como aggregation passar os valores na classe teste
        nodes = new ArrayList<Nodes>();
        edges = new ArrayList<Edge>();  
        
        for (int i = 0; i < 11; i++) {
            Nodes location = new Nodes("Node_" + i, "Node_" + i);
            nodes.add(location);
        }
        

        addLane("Edge_0", 0, 1, 85);
        addLane("Edge_1", 0, 2, 217); //esse
        addLane("Edge_2", 0, 4, 173);
        
        addLane("Edge_3", 2, 6, 186);
        addLane("Edge_4", 2, 7, 103); //esse
        
        addLane("Edge_5", 3, 7, 183);
        addLane("Edge_6", 5, 8, 250);
        addLane("Edge_7", 8, 9, 84);
        addLane("Edge_8", 7, 9, 167); // esse
        
        addLane("Edge_9", 4, 9, 502);
        addLane("Edge_10", 9, 10, 40);//esse
        
        addLane("Edge_11", 1, 10, 600); 

        
        Graph graph = new Graph(nodes, edges);
        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
       
        /* TESTANDO COM O PRIMEIRA LINHA DO NODE TIPO 1
         * First teste dijkstra.execute(nodes.get(0));
         */
        dijkstra.executar(source);
        
     
        System.out.println("Source:  "+ source);
        
        /* TESTANDO COM A ULTIMA LINHA DO NODE TIPO 10
         * First teste LinkedList<Nodes> path = dijkstra.getPath(nodes.get(10));
         */
        LinkedList<Nodes> path = dijkstra.getPath(nodes.get(10));

        System.out.println("Caminho:  "+ dijkstra.getPath(nodes.get(10)));

        for (Nodes vertex : path) {
            System.out.println(vertex);
        }

    }

    private void addLane(String laneId, int sourceLocNo, int destLocNo,
            int duration) {
        Edge lane = new Edge(laneId,nodes.get(sourceLocNo), nodes.get(destLocNo), duration );
        edges.add(lane);
    }
    

    
}