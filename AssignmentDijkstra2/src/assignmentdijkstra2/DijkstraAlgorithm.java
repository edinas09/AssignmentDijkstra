/*
* 
* import java.util.ArrayList;
*
*
*import java.util.Collections;
*
*
*
*import java.util.HashMap;
*
*
*import java.util.HashSet;
*
*
*import java.util.LinkedList;
*
*
*
*import java.util.List;
*
*
*
*import java.util.Map;
*
*
*
*import java.util.Set;
*
*
*
*/

import java.util.*;

/*Developer: Silva Edina
 *I didn't need to import the java could understand that I'm using Nodes, Edges and Graph
 */

//import Nodes;  
//import Edge; 
//import Graph;


public class DijkstraAlgorithm {

	//DECLARE ALL VARIABLES FIRST;
    public final List<Edge> edges;
    public Set<Nodes> visitedNodes; 
    public Set<Nodes> unVisitedNodes; 
    public Map<Nodes, Nodes> predecessors;
    public Map<Nodes, Integer> distance;

    
    //CONSTRUTOR - CREATING THE METHOD TO MAKE A COPY OF TWO LIST THAT I'M GONNA USE IT AND PUT AND THE SIMPLE LISSSSTTTT 
    public DijkstraAlgorithm(Graph graph) {
       
        this.edges = new ArrayList<Edge>(graph.getEdges());
    }

    //ESSE METODO E O MAIS IMPORTANTE  PQ E AONDE VC SETA OS VALORES INICIAIS DE TODAS AS VARIAS QUE VC VAI USAR!
    public void executar(Nodes source) {
        visitedNodes = new HashSet<Nodes>();
        unVisitedNodes = new HashSet<Nodes>();
        distance = new HashMap<Nodes, Integer>();
        predecessors = new HashMap<Nodes, Nodes>();
        distance.put(source, 0); 
        unVisitedNodes.add(source);
        //PERCORRE A LISTA DOS NAO VISITADOS E SETA OS NOVOS VALORES 
        while (unVisitedNodes.size() > 0 ) {
        	//PARA O NODE SEMPRE IREMOS COLOCAR O MINIMO ISSO SIGNIFICA QUE SERA O PROXIMO QUE SERA PERCORIDO
            Nodes node = getMinimum(unVisitedNodes); //FUNCAO PARA PEGAR O MINIMO
            visitedNodes.add(node);
            unVisitedNodes.remove(node); //SEMPRE TIRAR O NODE QUE ESTA SENDO VISITADO DA LISTA
            //ENCONTRA A DISTANCIA MINIMA NA LISTA DE NODES
            findMinimalDistances(node);
        }
    }

    //
    public void findMinimalDistances(Nodes node) {
      //CRIA A LISTA COLOCANDO O VIZINHO
    	List<Nodes> adjacentNodes = getNeighbors(node);
    	//ATRIBUI NA TARGET QUE E DO TIPO NODE O VIZINHO
        for (Nodes target : adjacentNodes) {   	
            if (getShortestDistance(target) > getShortestDistance(node)
                    + getDistance(node, target)) {
                distance.put(target, getShortestDistance(node)
                        + getDistance(node, target));
                predecessors.put(target, node);
                unVisitedNodes.add(target);
            }
        }
    }

    public int getDistance(Nodes node, Nodes target) {
        for (Edge edge : edges) {
            if (edge.getSource().equals(node)
                    && edge.getDestination().equals(target)) {
                return edge.getWeight();
            }
        }
       //I NEEDED TO PUT IT BECAUSE I DECIDE TO USE O RETURN INSIDE OF THE "IF" SOOOO WEIRD I SPEND A LOT OF TIME TRY TO DO IT 
        throw new RuntimeException("Should not happen");
    }

    public List<Nodes> getNeighbors(Nodes node) {
        List<Nodes> neighbors = new ArrayList<Nodes>();
        for (Edge edge : edges) {
            if (edge.getSource().equals(node)
                    && !isSettled(edge.getDestination())) {
                neighbors.add(edge.getDestination());
            }
        }
        return neighbors;
    }

    public Nodes getMinimum(Set<Nodes> vertexes) {
        Nodes minimum = null;
        for (Nodes vertex : vertexes) {
            if (minimum == null) {
                minimum = vertex;
            } else {
                if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
                    minimum = vertex;
                }
            }
        }
        return minimum;
    }

    public boolean isSettled(Nodes vertex) {
        return visitedNodes.contains(vertex);
    }

    public int getShortestDistance(Nodes destination) {
        Integer d = distance.get(destination);
        if (d == null) {
            return Integer.MAX_VALUE;
        } else {
            return d;
        }
    }

    /*
     * This method returns the path from the source to the selected target and
     * NULL if no path exists
     */
    public LinkedList<Nodes> getPath(Nodes target) {
        LinkedList<Nodes> path = new LinkedList<Nodes>();
        Nodes step = target;
        // check if a path exists
        if (predecessors.get(step) == null) {
            return null;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        // Put it into the correct order
        Collections.reverse(path);
        return path;
    }
}