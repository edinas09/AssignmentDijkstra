public class Nodes {
    final private String id;
    final private String name;

    //CRIANDO A ESTRUTURA DA CLASE
    public Nodes(String id, String name) {
        this.id = id;
        this.name = name;
    }
    
    //CRIANDO OS GETS DA VIDA
    public String getId() {return id;}
    public String getName() {return name;}

    
    // VALIDA SE O NUMERO E EQUAL
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Nodes other = (Nodes) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {return name; }

}
