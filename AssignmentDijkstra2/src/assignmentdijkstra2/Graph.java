//I NEED TO IMPORT LIBRARY TO USE LIST
import java.util.List;

public class Graph {
	
	//criando as listas de nodes e de Edges
    public final List<Nodes> matrix;
    public final List<Edge> edges;

    //criando a estrutura da classe
    public Graph(List<Nodes> matrix,
    		List<Edge> edges) {
        this.matrix = matrix;
        this.edges = edges;
    }

    //criando 2 metodos GETs
   public List<Nodes> getNodes() {return matrix;}
   public List<Edge> getEdges() {return edges;}



}