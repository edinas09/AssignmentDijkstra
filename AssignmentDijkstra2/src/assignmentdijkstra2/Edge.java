public class Edge  {
	/*Developer: Edina 
	 *DataComents: 20/06 i put final after explanation
	 *Data Coments:  
	 * 
	 */
    private final String id; //MY INDEX - I put final because its never change NAO ESTOU USANDO LIST OU ARRAY AQUI OU SEJA ID E O MEU INDEX
    private final Nodes source; // never change
    private final Nodes destination; // never change
    private final int weight;//never change 
    
    
    /* at least i was doing like thar when i decided create to share and create every thing in class
     * 	List<String[]> list = new ArrayList<String[]>();
     *  String[] nodes = {"A","B","C","D","E", "F"}; 
     *  String[] unvisitnodes = nodes; 
     *  list.add(nodes);
     *  list.add(unvisitnodes);
		for (String[] StrArr : list){			
			System.out.println(Arrays.toString(StrArr));
		}
     * 
     * 
     */
    
    // CRIANDO A ESTRUTURA DA CLASSE
    public Edge(String id, Nodes source, Nodes destination, int weight) {
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    } 
    
   

    //CRIANDO OS GETS
    public String getId() { return id; }
    public Nodes getDestination() {   return destination;}
    public Nodes getSource() { return source; }
    public int getWeight() {return weight;}

    //SOMENTE SERA USANDO NESSA CLASSE THAT'S WHY IS OVERRIDE!
    @Override
    public String toString() {return source + " " + destination;}


}