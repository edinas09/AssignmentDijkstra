/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author edina
 */
public class ConditionalOperator {
    
    public static void main(String args[]){
        int a, b;
    a = 10;
    b = (a == 1)? 20:30; // if a == 1 then value of b became 20 otherwise became 30
    
    System.out.println("Value of b is: "+ b); //then is 30 because the value of a is 10 and is different
    
    b = (a == 10)? 20:30; // if a == 10 the value of b became 20 otherwise became 30
    System.out.println("Value of b is: " + b); //then is 20 because the value of a is 10
    
    } 
    
    
}
