/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author edina
 */
public class ArithmeticOperators {
    
    public static void main (String args[])
    {
    int a = 10;
    int b = 20;
    int c = 25;
    int d = 25;
    
    System.out.println ("a + b = " + (a+b) );
    System.out.println ("a - b = " + (a-b) );
    System.out.println ("a * b = " + (a*b) );
    System.out.println ("a / b = " + (a/b) );
    System.out.println ("a % b = " + (a%b) );
    System.out.println ("c % a = " + (c%a) );
    System.out.println ("a ++  = " + (a++) );
    System.out.println ("b --  = " + (b--) );
    
    //Check the diferrence in d++ and ++d      
    System.out.println ("d++ = " + (d++) );// just add 1 number it means that know is 26. to use next time that's why that show the same value 25.
    System.out.println ("++d = " + (++d) );    // now the value is 26 so add + 1 and use the new value 27.
    
    }
    
    
}
